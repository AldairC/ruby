# frozen_string_literal: true

COLORS = %w[black brown red orange yellow green blue violet grey white].freeze

# Exercism resistor color duo
class ResistorColorDuo
  def self.value(colors)
    "#{COLORS.index(colors[0])}#{COLORS.index(colors[1])}".to_i
  end
end

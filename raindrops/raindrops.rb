# frozen_string_literal: true

RAINDROPS = { 3 => 'Pling', 5 => 'Plang', 7 => 'Plong' }.freeze

# Exercism raindrops
class Raindrops
  def self.convert(number)
    result = RAINDROPS.map { |x, value| value if (number % x).zero? }.join
    result == '' ? number.to_s : result
  end
end

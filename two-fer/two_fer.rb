# frozen_string_literal: true

# Exercism Two Fer
class TwoFer
  def self.two_fer(name = 'you')
    "One for #{name}, one for me."
  end
end

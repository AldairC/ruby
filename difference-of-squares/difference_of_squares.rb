# frozen_string_literal: true

# Exercism difference of squares
class Squares
  attr_reader :number

  def initialize(number)
    @number = number
  end

  def square_of_sum
    (1..number).to_a.reduce(0, :+)**2
  end

  def sum_of_squares
    (1..number).to_a.map { |x| x**2 }.reduce(0, :+)
  end

  def difference
    square_of_sum - sum_of_squares
  end
end

# frozen_string_literal: true

# Exercism sum of multiples
class SumOfMultiples
  attr_reader :args

  def initialize(*args)
    @args = args
  end

  def to
    args
  end
end